/*
 *
 * jQuery.eeqMask v1.0
 * http://git.oschina.net/eeq/jQuery.eeqMask
 * 
 * Copyright (c) 2014 @Eeqlee
 * Licensed under the MIT.
 *
 */
 
(function($) {

	$.eeqMask = {
		settings:$.extend({
			textBoxId : 'maskText',
			textColor : '#fff',
			textBgColor : '#000',
			textSize : '16px',
			textBoxWidth : 200,
			textBoxHeight : 50,
			textBoxBorderCrude : 1,
			textBoxBorderStyle : 'solid',
			textBoxBorderColor : '#ddd',
			textBoxZ : 99999,
			textBoxText : 'Loading<span>...</span>', //if this value set false, then the text box will not show.
			maskBoxId : 'maskBg',
			maskBgColor : '#000',
			maskOpacity : 6,
			maskZ : 99998
		}, {}),
		set:function(options){
			this.settings = $.extend(this.settings, options);
		},
		create:function(){
			var $body = $('body:eq(0)');
			var textBox = '<div id="'+this.settings.textBoxId+'">'+this.settings.textBoxText+'</div>';
			var maskBox = '<div id="'+this.settings.maskBoxId+'"></div>';
			$body.append(textBox,maskBox);
			
			$('#'+this.settings.textBoxId).css({
				'background-color':this.settings.textBgColor,
				color:this.settings.textColor,
				'font-size':this.settings.textSize,
				'font-weight':'bold',
				'text-align':'center',
				width:this.settings.textBoxWidth,
				height:this.settings.textBoxHeight,
				'line-height':this.settings.textBoxHeight + 'px',
				border:this.settings.textBoxBorderCrude + 'px ' + this.settings.textBoxBorderStyle +' '+ this.settings.textBoxBorderColor,
				left:'50%',
				'margin-left':'-'+ this.settings.textBoxWidth/2 + 'px',
				position:'absolute',
				'z-index':this.settings.textBoxZ,
				display:'none'
			});
			
			var maskWidth = $(window).width();
			var maskHeight = $(window).height();
			if($body.height()>$(window).height()){
				maskHeight = $body.height();
			}
			$('#'+this.settings.maskBoxId).css({
				'background-color':this.settings.maskBgColor,
				width:maskWidth,
				height:maskHeight,
				top:0,
				left:0,
				filter:'alpha(opacity='+this.settings.maskOpacity+'0)',
				'-moz-opacity':'0.'+this.settings.maskOpacity,
				opacity: '0.'+this.settings.maskOpacity,
				position:'absolute',
				'z-index':this.settings.maskZ,
				display:'none'
			});
		},
		open:function(backFunc){
			if(!backFunc){
				var backFunc = function(){};
			}
			if($('#'+this.settings.textBoxId).length == 0){
				this.create();
			}
			if(this.settings.textBoxText){
				var textBoxTop = '50%';
				var textBoxMgTop = '-'+ this.settings.textBoxHeight/2 + 'px';
				if($(window).scrollTop() > 0){
					textBoxTop = $(window).scrollTop() + ($(window).height()/2);
					textBoxMgTop = 0;
				}
				$('#'+this.settings.textBoxId).css({
					top:textBoxTop,
					'margin-top':textBoxMgTop
				}).show();
			}
			$('#'+this.settings.maskBoxId).show();
			backFunc();
		},
		close:function(backFunc){
			if(!backFunc){
				var backFunc = function(){};
			}
			if($('#'+this.settings.textBoxId).length != 0){
				if(this.settings.textBoxText){
					$('#'+this.settings.textBoxId).hide();
				}
				$('#'+this.settings.maskBoxId).hide();
			}
			backFunc();
		}
	};
	
})(jQuery);